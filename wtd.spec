Name:		wtd
Summary:	A systemd timer sending heartbeats to the Watchdog Pro2/Mini
Summary(ru_RU.UTF-8): Таймер systemd для Watchdog Pro2/Mini
Version:	1.1.1
Release:	1%{?dist}
License:	Public Domain
URL:		https://gitlab.com/pda_rpms/wtd
Group:		System Environment/Daemons
Packager:	Dmitriy Pomerantsev <pda2@yandex.ru>
Source0:	wtd
Source1:	wtd.conf
Source2:	wtd.rules
Source3:	wtd.service
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
%{?systemd_requires}
%if 0%{?rhel} == 7
Requires:	systemd
%else
Requires:	systemd-udev
%endif
Requires:	coreutils gawk
%if 0%{?rhel} > 8 || 0%{?fedora} >= 30
BuildRequires:	systemd-rpm-macros
%endif

%description
Watchdog Pro2 / Mini is a small usb device, a watchdog timer that allows you
to restart your computer if it freezes. This systemd timer sends hearbeats,
notifying the device that the computer is not freezed.
See https://open-dev.ru/wdt-manual

%description -l ru_RU.UTF-8
Watchdog Pro2/Mini - небольшое usb устройство, сторожевой таймер, позволающий
перезагрузить компьютер, если он завис. Этот таймер systemd отправляет в него
ежесекундные импульсы, уведомляющие устройство о том, что компьютер не завис.
См. https://open-dev.ru/wdt-manual

%build
# None required

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_prefix}/bin
%{__install} -pm 0744 %{SOURCE0} %{buildroot}%{_prefix}/bin/

%{__mkdir_p} %{buildroot}%{_sysconfdir}/sysconfig
%{__install} -pm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/wtd

%{__mkdir_p} %{buildroot}%{_udevrulesdir}
%{__install} -pm 0644 %{SOURCE2} %{buildroot}%{_udevrulesdir}/

%{__mkdir_p} %{buildroot}%{_unitdir}
%{__install} -pm 0644 %{SOURCE3} %{buildroot}%{_unitdir}/

%post
udevadm control --reload-rules
udevadm trigger --subsystem-match='tty'

%postun
udevadm control --reload-rules
udevadm trigger --subsystem-match='tty'

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%{_prefix}/bin/*
%{_udevrulesdir}/*
%{_unitdir}/*

%changelog
* Fri Jan 31 2020 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.1.1 - 1
- Fixed service unit file.

* Sat Jan 19 2020 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.1.0 - 1
- Daemon now starts a bit earlier, before SELinux relabeling.
- Daemon now does not require r/w root filesystem.

* Mon Dec 02 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.1 - 2
- Added build dependency.

* Mon Dec 02 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.1 - 1
- Systemd timer replaced to shell daemon because of log spamming.
- Service unit moved to system location.
- Udev rules moved to system location.

* Fri Nov 29 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 1
- Initial package.
